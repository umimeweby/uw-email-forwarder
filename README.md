# UW Email Forwarder Wordpress plugin

Wordpress plugin that provides function to set up additional addresses for every email WP send.
You can add to existing or overwrite them.
TO, CC and BCC 

## Getting started - Základní úvod
 
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Instrukce, jak rozběhnout projekt na vývojovém prostředí (lokální PC , demo server) pro účely dalšího vývoje a testování.

Popis, jak zprovoznit projekt na produkčním prostředí jsou v sekci `Deployment`

### Prerequisites - Předpoklady

What things you need to install the software and how to install them

Composer - https://getcomposer.org



### Installing - Instalace 

A step by step series of examples that tell you how to get a development env running


**development**

Typický postup je tento:
1. V lokálním wordpressu se přesunout do složky wp-content/plugins
2. Klonovat plugin z GITu do tohoto adresáře, např `git clone git@bitbucket.org:umimeweby/uw-email-forwarder.git`
3. Přejít do adresáře pluginu, např. `cd uw-email-forwarder`
4. Spustit `composer install` pro instalaci potřebných knihoven
5. Spustit `composer dump-autoload -o` pro vytvoření autoload souboru pro nahrávání namespaces
6. Aktivovat plugin v seznamu pluginů ve Wordpress administraci


## Running the tests - Spouštění testů

No tests 

### And coding style tests  -- zahrňte i testy pro coding style standards

No code style rules

## Deployment - instalace na produkční prostředí

Pro instalaci na produkční  prostředí je nejlepší připravit plugin na lokálním wordpressu, viz body 1-6 výše v sekci development.


Následně :

1. Pomocí ftp umístit plugin na web klienta
2. Neuploadovat tam složku `.git` , pokud nahraná pak smazat
3. Aktivovat plugin

Otestovat že plugin funguje.


## Usage - návod na použití

Instructions how to use the project if needed, or link to user documentaion 

Po aktivaci pluginu je v administraci v Menu Nastavení (Settings) volba UW Email Forwarder, kde je možné nastavit požadované email adresy a zda se máí přidávat nebo mají přepsat původní adresy.

Zde screenshot z administrace

![image text](screenshot_uef.png "UW Email Forwarder admin screenshot")


## Built With - Co bylo použito při vývoji projektu

* [PHP](https://php.net/) - The programming language used
* [Wordpress](https://wordpress.org/) - The CMS framework used
* [Composer](https://getcomposer.org/) - Dependency Management
* [Wordpress Plugin Boilerplate](https://wppb.io/) - Plugin boilerplate
* [Wordpress Plugin Boilerplate Generator](https://wppb.me/) - Plugin skeleton generator

## Contributing - Zapojení do projektu

Use Pull request to add code to project


## Versioning - verzování

We use [GIT](https://git-scm.com/) for versioning and Bitbucket for cloude repository. 



## Authors - Autoři a spolupracovníci

* **Richard Koza** - [Umimeweby](https://www.umimeweby.cz/)


## Acknowledgments - Poděkování


* [Wordpress Plugin Boilerplate Powered](https://wpbp.github.io/) - another plugin boilerplate

