<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       www.umimeweby.cz
 * @since      1.0.0
 *
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/includes
 * @author     Richard Koza <info@umimeweby.cz>
 */
class Uw_Email_Forwarder_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'uw-email-forwarder',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
