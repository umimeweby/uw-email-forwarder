<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.umimeweby.cz
 * @since      1.0.0
 *
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/includes
 * @author     Richard Koza <info@umimeweby.cz>
 */
class Uw_Email_Forwarder_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
