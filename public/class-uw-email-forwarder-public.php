<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       www.umimeweby.cz
 * @since      1.0.0
 *
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/public
 */

use UEF_Umimeweby\Mail_Filter\Email_Addresses_Modificator;

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/public
 * @author     Richard Koza <info@umimeweby.cz>
 */
class Uw_Email_Forwarder_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;


    private $email_addresses_modificator;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->email_addresses_modificator = new Email_Addresses_Modificator();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Uw_Email_Forwarder_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Uw_Email_Forwarder_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/uw-email-forwarder-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Uw_Email_Forwarder_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Uw_Email_Forwarder_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/uw-email-forwarder-public.js', array( 'jquery' ), $this->version, false );

	}


    public function check_and_modify_email_if_needed($args)
    {
        return $this->email_addresses_modificator->modify_emails($args);
    }

    public function handle_phpmailer_addresses($phpmailer)
    {
        return $this->email_addresses_modificator->handle_cc_bcc_addresses($phpmailer);
    }

}
