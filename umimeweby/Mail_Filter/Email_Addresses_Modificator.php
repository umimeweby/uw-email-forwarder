<?php

namespace UEF_Umimeweby\Mail_Filter;

use UEF_Umimeweby\Options\Options_Settings_Manager;

class Email_Addresses_Modificator
{


    /**
     *
     * Modify To email address based on stored options
     *
     * @param mixed $args
     * @return mixed
     */
    public function modify_emails($args)
    {

        // handling TO addresses
        $to = $args['to'];
        $overwrite_to = get_option(Options_Settings_Manager::SETTING_FIELD_TO_EMAILS_OVER);
        if ($overwrite_to === false || trim($overwrite_to) == '') {
            $overwrite_to = '0';
        }

        $new_to = get_option(Options_Settings_Manager::SETTING_FIELD_TO_EMAILS);
        if (!($new_to === false || trim($new_to) == '')) {
            if ($overwrite_to == '1') {
                $args['to'] = $new_to;
            } else {
                $args['to'] = $to.','.$new_to;
            }
        }
        return $args;

    }


    /**
     * Modify cc and bcc addresses based on stored options
     *
     * @param [type] $phpmailer
     * @return void
     */
    public function handle_cc_bcc_addresses($phpmailer)
    {
        $cc_adresses = $phpmailer->getCcAddresses();
        $bcc_addresses = $phpmailer->getBccAddresses();

        // Handling of CC

        $overwrite_cc = get_option(Options_Settings_Manager::SETTING_FIELD_CC_EMAILS_OVER);
        if ($overwrite_cc === false || trim($overwrite_cc) == '') {
            $overwrite_cc = '0';
        }

        $new_cc = get_option(Options_Settings_Manager::SETTING_FIELD_CC_EMAILS);
        if (!($new_cc === false || trim($new_cc) == '')) {
            if ($overwrite_cc == '1') {
                $phpmailer->clearCCs();
            }
            $new_cc_array = explode(',',$new_cc);
            foreach ($new_cc_array as $one_new_cc)
            {
                $phpmailer->addCC($one_new_cc);
            }
        }

        // Handling of BCC


        $overwrite_bcc = get_option(Options_Settings_Manager::SETTING_FIELD_BCC_EMAILS_OVER);
        if ($overwrite_bcc === false || trim($overwrite_bcc) == '') {
            $overwrite_bcc = '0';
        }

        $new_bcc = get_option(Options_Settings_Manager::SETTING_FIELD_BCC_EMAILS);
        if (!($new_bcc === false || trim($new_bcc) == '')) {
            if ($overwrite_bcc == '1') {
                $phpmailer->clearBCCs();
            }
            $new_bcc_array = explode(',',$new_bcc);
            foreach ($new_bcc_array as $one_new_bcc)
            {
                $phpmailer->addBCC($one_new_bcc);
            }
        }

    }
}
