<?php
namespace UEF_Umimeweby\Options;

class Options_Page_Manager
{

    private $plugin_name;

    private $plugin_version;

    public function __construct(string $plugin_name, string $plugin_version)
    {
        $this->plugin_name = $plugin_name;
        $this->plugin_version = $plugin_version;
    }

    public function add_options_page_to_admin_menu()
    {
        add_submenu_page(
            'options-general.php',
            'UW Email Forwarder page title',
            'UW Email Forwarder',
            'administrator',
            UW_EMAIL_FORWARDER_NAME,
            [$this, 'display_plugin_menu_dashboard']
            );
    }

    public function display_plugin_menu_dashboard()
    {
        $content = '';

        // check if the user have submitted the settings
        // WordPress will add the "settings-updated" $_GET parameter to the url
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            add_settings_error( $this->plugin_name.'_messages', 'wporg_message', __( 'Settings Saved', $this->plugin_name ), 'updated' );
        }

        // show error/update messages
        settings_errors( $this->plugin_name.'_messages' );

        $content .= '<div class="wrap">';
        $content .= '<h1>Umimeweby Email Forwarder Settings</h1>';
        $content .= '<form action="options.php" method="post">';
        echo $content;

        // output security fields for the registered setting "wporg_options"
        settings_fields( $this->plugin_name );
        // output setting sections and their fields
        // (sections are registered for $this->plugin_name value, each field is registered to a specific section)
        do_settings_sections( $this->plugin_name );
        // output save settings button
        submit_button( __( 'Save Settings', 'textdomain' ) );

        $content = '';
        $content .= '</form>';
        $content .= '</div><!-- wrap -->';
        $content .= '';
        echo $content;



    }
}

