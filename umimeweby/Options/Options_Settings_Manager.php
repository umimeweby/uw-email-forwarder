<?php

namespace UEF_Umimeweby\Options;

class Options_Settings_Manager
{

    const SECTION_EMAILS_TO = 'uw_email_forwarder_emails_section_to';
    const SECTION_EMAILS_CC = 'uw_email_forwarder_emails_section_cc';
    const SECTION_EMAILS_BCC = 'uw_email_forwarder_emails_section_bcc';

    const SETTING_FIELD_TO_EMAILS = 'uw_email_forwarder_to_emails';
    const SETTING_FIELD_TO_EMAILS_OVER = 'uw_email_forwarder_to_emails_over';
    const SETTING_FIELD_CC_EMAILS = 'uw_email_forwarder_cc_emails';
    const SETTING_FIELD_CC_EMAILS_OVER = 'uw_email_forwarder_cc_emails_over';
    const SETTING_FIELD_BCC_EMAILS = 'uw_email_forwarder_bcc_emails';
    const SETTING_FIELD_BCC_EMAILS_OVER = 'uw_email_forwarder_bcc_emails_over';

    private $plugin_name;

    private $plugin_version;

    public function __construct(string $plugin_name, string $plugin_version)
    {
        $this->plugin_name = $plugin_name;
        $this->plugin_version = $plugin_version;
    }


    public function add_settings_sections_and_fields()
    {
        add_settings_section(
            self::SECTION_EMAILS_TO,
            'Email address settings for TO adresses',
            null,
            $this->plugin_name
        );
        add_settings_section(
            self::SECTION_EMAILS_CC,
            'Email address settings for CC adresses',
            null,
            $this->plugin_name
        );
        add_settings_section(
            self::SECTION_EMAILS_BCC,
            'Email address settings for BCC adresses',
            null,
            $this->plugin_name
        );

        add_settings_field(
            self::SETTING_FIELD_TO_EMAILS,
            'TO addresses',
            [$this, 'render_field_to_emails'],
            $this->plugin_name,
            self::SECTION_EMAILS_TO,
            [
                'label_for' => self::SETTING_FIELD_TO_EMAILS,
                'class' => 'uef_textfield_emails',
            ]
        );

        add_settings_field(
            self::SETTING_FIELD_CC_EMAILS,
            'CC addresses',
            [$this, 'render_field_cc_emails'],
            $this->plugin_name,
            self::SECTION_EMAILS_CC,
            [
                'label_for' => self::SETTING_FIELD_CC_EMAILS,
                'class' => 'uef_textfield_emails',
            ]
        );
        add_settings_field(
            self::SETTING_FIELD_BCC_EMAILS,
            'BCC addresses',
            [$this, 'render_field_bcc_emails'],
            $this->plugin_name,
            self::SECTION_EMAILS_BCC,
            [
                'label_for' => self::SETTING_FIELD_BCC_EMAILS,
                'class' => 'uef_textfield_emails',
            ]
        );

        add_settings_field(
            self::SETTING_FIELD_TO_EMAILS_OVER,
            'Overwrite TO address ?',
            [$this, 'render_field_to_emails_over'],
            $this->plugin_name,
            self::SECTION_EMAILS_TO,
            [
                'label_for' => self::SETTING_FIELD_TO_EMAILS_OVER,

            ]
        );

        add_settings_field(
            self::SETTING_FIELD_CC_EMAILS_OVER,
            'Overwrite CC address ?',
            [$this, 'render_field_cc_emails_over'],
            $this->plugin_name,
            self::SECTION_EMAILS_CC,
            [
                'label_for' => self::SETTING_FIELD_CC_EMAILS_OVER,

            ]
        );

        add_settings_field(
            self::SETTING_FIELD_BCC_EMAILS_OVER,
            'Overwrite BCC address ?',
            [$this, 'render_field_bcc_emails_over'],
            $this->plugin_name,
            self::SECTION_EMAILS_BCC,
            [
                'label_for' => self::SETTING_FIELD_BCC_EMAILS_OVER,

            ]
        );


        register_setting($this->plugin_name, self::SETTING_FIELD_TO_EMAILS, [
            'type' => 'string'
        ]);
        register_setting($this->plugin_name, self::SETTING_FIELD_TO_EMAILS_OVER, [
            'type' => 'boolean'
        ]);
        register_setting($this->plugin_name, self::SETTING_FIELD_CC_EMAILS, [
            'type' => 'string'
        ]);
        register_setting($this->plugin_name, self::SETTING_FIELD_CC_EMAILS_OVER, [
            'type' => 'boolean'
        ]);
        register_setting($this->plugin_name, self::SETTING_FIELD_BCC_EMAILS, [
            'type' => 'string'
        ]);
        register_setting($this->plugin_name, self::SETTING_FIELD_BCC_EMAILS_OVER, [
            'type' => 'boolean'
        ]);

    }




    public function invitation_setting_section_callback_function($arg)
    {
        // echo section intro text here
        echo '<p>Email addresses section</p>';
    }


    public function render_field_to_emails($args)
    {
        // get the value of the setting we've registered with register_setting()
        $setting = get_option(self::SETTING_FIELD_TO_EMAILS);
        if ($setting === false || trim($setting) == '') {
            $setting = '';
        }
        $content = '<input type="text" id="' . self::SETTING_FIELD_TO_EMAILS . '" name="' . self::SETTING_FIELD_TO_EMAILS . '" value="' . $setting . '">';
        $content .= '<br>';
        $content .= '<small>You can write more addresses separated by comma</small>';
        echo $content;
    }

    public function render_field_to_emails_over($args)
    {
        // get the value of the setting we've registered with register_setting()
        $setting = get_option(self::SETTING_FIELD_TO_EMAILS_OVER);
        $content = '<input type="checkbox" id="' . self::SETTING_FIELD_TO_EMAILS_OVER . '" name="' . self::SETTING_FIELD_TO_EMAILS_OVER . '" value="1"'.checked( 1, $setting, false ).'>';
        $content .= '<label for="'.self::SETTING_FIELD_TO_EMAILS_OVER.'">Overwrite TO addresses with your input above?</label>';
        echo $content;
    }

    public function render_field_cc_emails($args)
    {
        // get the value of the setting we've registered with register_setting()
        $setting = get_option(self::SETTING_FIELD_CC_EMAILS);
        if ($setting === false || trim($setting) == '') {
            $setting = '';
        }
        $content = '<input type="text" id="' . self::SETTING_FIELD_CC_EMAILS . '" name="' . self::SETTING_FIELD_CC_EMAILS . '" value="' . $setting . '">';
        $content .= '<br>';
        $content .= '<small>You can write more addresses separated by comma</small>';
        echo $content;
    }

    public function render_field_cc_emails_over($args)
    {
        // get the value of the setting we've registered with register_setting()
        $setting = get_option(self::SETTING_FIELD_CC_EMAILS_OVER);
        $content = '<input type="checkbox" id="' . self::SETTING_FIELD_CC_EMAILS_OVER . '" name="' . self::SETTING_FIELD_CC_EMAILS_OVER . '" value="1"'.checked( 1, $setting, false ).'>';
        $content .= '<label for="'.self::SETTING_FIELD_CC_EMAILS_OVER.'">Overwrite CC addresses with your input above?</label>';
        echo $content;
    }

    public function render_field_bcc_emails($args)
    {
        // get the value of the setting we've registered with register_setting()
        $setting = get_option(self::SETTING_FIELD_BCC_EMAILS);
        if ($setting === false || trim($setting) == '') {
            $setting = '';
        }
        $content = '<input type="text" id="' . self::SETTING_FIELD_BCC_EMAILS . '" name="' . self::SETTING_FIELD_BCC_EMAILS . '" value="' . $setting . '">';
        $content .= '<br>';
        $content .= '<small>You can write more addresses separated by comma</small>';
        echo $content;
    }
    public function render_field_bcc_emails_over($args)
    {
        // get the value of the setting we've registered with register_setting()
        $setting = get_option(self::SETTING_FIELD_BCC_EMAILS_OVER);
        $content = '<input type="checkbox" id="' . self::SETTING_FIELD_BCC_EMAILS_OVER . '" name="' . self::SETTING_FIELD_BCC_EMAILS_OVER . '" value="1"'.checked( 1, $setting, false ).'>';
        $content .= '<label for="'.self::SETTING_FIELD_BCC_EMAILS_OVER.'">Overwrite BCC addresses with your input above?</label>';
        echo $content;
    }

}
