<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www.umimeweby.cz
 * @since      1.0.0
 *
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/admin
 */

use UEF_Umimeweby\Options\Options_Page_Manager;
use UEF_Umimeweby\Options\Options_Settings_Manager;

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/admin
 * @author     Richard Koza <info@umimeweby.cz>
 */
class Uw_Email_Forwarder_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    private $options_page_manager;

    private $options_settings_manager;



	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->options_page_manager =  new Options_Page_Manager($this->plugin_name, $this->version);
        $this->options_settings_manager = new Options_Settings_Manager($this->plugin_name, $this->version);

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Uw_Email_Forwarder_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Uw_Email_Forwarder_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/uw-email-forwarder-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Uw_Email_Forwarder_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Uw_Email_Forwarder_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/uw-email-forwarder-admin.js', array( 'jquery' ), $this->version, false );

	}


    /**
	 * Options and Settings pages for plugin
	 */

	/**
	 * Register Plugin Options and setting pages in admin
	 */
	public function register_settings_pages_and_menu_items()
	{
	    $this->options_page_manager->add_options_page_to_admin_menu();
	}

	/**
	 *
	 * Adds all settings sections and fields and register settings for plugin
	 *
	 */
	public function add_settings_and_sections_and_fields()
	{
	    $this->options_settings_manager->add_settings_sections_and_fields();
	}

}
