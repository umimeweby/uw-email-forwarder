<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       www.umimeweby.cz
 * @since      1.0.0
 *
 * @package    Uw_Email_Forwarder
 * @subpackage Uw_Email_Forwarder/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
