<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.umimeweby.cz
 * @since             1.0.0
 * @package           Uw_Email_Forwarder
 *
 * @wordpress-plugin
 * Plugin Name:       UW Email Forwarder
 * Plugin URI:        www.umimeweby.cz
 * Description:       Send all emails to specific To, CC or BCC adresses
 * Version:           1.0.0
 * Author:            Richard Koza
 * Author URI:        www.umimeweby.cz
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       uw-email-forwarder
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'UW_EMAIL_FORWARDER_VERSION', '1.0.0' );
define( 'UW_EMAIL_FORWARDER_TEXTDOMAIN', 'uw-email-forwarder' );
define( 'UW_EMAIL_FORWARDER_NAME', 'uw-email-forwarder' );
define( 'UW_EMAIL_FORWARDER_PLUGIN_ROOT', plugin_dir_path( __FILE__ ) );
define( 'UW_EMAIL_FORWARDER_PLUGIN_ABSOLUTE', __FILE__ );


/**
 *
 * Disable plugin for PHP less than 7.4.0
 *
 */
if ( version_compare( PHP_VERSION, '7.4.0', '<=' ) ) {
    add_action(
        'admin_init',
        static function() {
            deactivate_plugins( plugin_basename( __FILE__ ) );
        }
        );
    add_action(
        'admin_notices',
        static function() {
            echo wp_kses_post(
                sprintf(
                    '<div class="notice notice-error"><p>%s</p></div>',
                    __( '"uw-paywall" requires PHP 7.4 or newer.', UW_EMAIL_FORWARDER_TEXTDOMAIN )
                    )
                );
        }
        );

    // Return early to prevent loading the plugin.
    return;
}

/**
 * The Composer Autoloader function to handle Umimeweby namespace and dependencies
 */
require_once plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';




/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-uw-email-forwarder-activator.php
 */
function activate_uw_email_forwarder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-uw-email-forwarder-activator.php';
	Uw_Email_Forwarder_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-uw-email-forwarder-deactivator.php
 */
function deactivate_uw_email_forwarder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-uw-email-forwarder-deactivator.php';
	Uw_Email_Forwarder_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_uw_email_forwarder' );
register_deactivation_hook( __FILE__, 'deactivate_uw_email_forwarder' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-uw-email-forwarder.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_uw_email_forwarder() {

	$plugin = new Uw_Email_Forwarder();
	$plugin->run();

}
run_uw_email_forwarder();
